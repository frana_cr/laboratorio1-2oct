## EJERCICIO 2 " DETERMINAR MINUSCULAS Y MAYUSCULAS "
La finalidad es poder crear un programa con lenguaje C++ que permita por medio de las clases y arreglos encontrar las letras mayusculas y minusculas, este arreglo puede leer N frases y todas tendran el tipo caracter. 
El programa se desarrollo principalmente con la clase contador, se definio la clase y los atributos de esta, se genero el archivo.h el cual es el encargado de ser el archivo base o cabecera del archivo Contador.cpp, tambien se definio el archivo Programa.cpp en donde se llaman a las funciones y ademas se genera la interaccion con el usuario.
- En el archivo Contador.h se genera la clase, sus atributos tanto publicos como privados y tambien los constructores con sus parametros, se declaran tres funciones principales una que complete el arreglo, una que cuente letras minusculas y mayusculas, este archivo logra compilar con exito.
- En el archivo Contador.cpp se llama al archivo cabecera se definen los get y set, estos procesos funcionan correctamente. Se crea la funcion completar_arreglo() en la cual mediante el tamaño del arreglo sean agregadas las frases que el usuario ingrese, luego en la siguiente funcion contar_mayusculas() se lleva a cabo una condicion para que identifique y se agregue a un contador cuando encuentre una letra con estas caracteristicas esta funcion es similar al contar_minusculas, solamente difieren en que cuentan mayusculas y minusculas. este programa presenta errores en su compilacion y se cree que puede ser debido a un problema en la funcion completar_arreglo().
- El archivo Programa.cpp imprime segun estas funciones las mayusculas y minusculas segun corresponda para eso, se imprime el anuncio al usuario y se llama a cada clase correspondiente, al presentar errores en las clases no se imprime con exito, pero al probar solamente con cout funciona de buena manera. 
## Pre-requisitos
Sistema operativo Linux versión igual o superior a 18.04
Editor de texto (vim o geany)

### Instalación
Para poder ejecutar el programa se debe conocer que versión de Ubuntu presenta la computadora. Ejecutar este comando:
lsb_release -a (versión de Ubuntu)
En donde:
Se puede corroborar qué versión se tiene instalada actualmente, debido a que se pueden presentar problemas si esta no es compatible con la aplicación que se está desarrollando.
Para descargar un editor de texto como vim se puede descargar de la siguiente manera:
sudo apt install vim
En donde:
Por medio de este editor de texto se puede construir el codigo

En el caso del desarrollo del trabajo se implemento el editor de texto Geany, descargado de Ubuntu Software.


### Ejecutando las pruebas
Para implementar la ejecución del código, colocar en terminal: g++ Programa -o Programa o por medio del archivo Makefile.

Cada archivo se puede ejecutar para ver si tiene errores por medio del comando en terminal g++ Nombre del archivo.cpp -o Nombre Archivo.


##Construido con:
Ubuntu: Sistema operativo.
C++: Lenguaje de programación.
Geany: Editor de código.

## Versiones
Versiones de herramientas:
Ubuntu 20.04 LTS
Geany 1.36-1build1
Versiones del desarrollo del codigo: https://gitlab.com/frana_cr/laboratorio1-2oct/-/tree/master/Ejercicio%202

## Autores
Francisca Castillo - Desarrollo del código y proyecto, narración README.

## Expresiones de Gratitud
A los ejemplos en la plataforma de Educandus de Alejandro Valdes: https://lms.educandus.cl/mod/lesson/view.php?id=730534
Recursos web: http://www.cplusplus.com/reference/cctype/toupper/


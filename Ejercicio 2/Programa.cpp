/*
  * g++ Programa.cpp -o Contador.cpp
 */
#include <iostream>
#include <ctype.h> 
using namespace std;
#include "Contador.h"

int main (int argc, char **argv) {
	int frases = stoi(argv[1]);
	cout << "BUSCADOR DE MAYUSCULAS Y MINUSCULAS" << endl;
	cout << "LAS MAYUSCULAS SON:"<< contar_mayusculas() << endl;
	cout << "LAS MINUSCULAS SON:" << contar_minusculas() << endl;
	
    return 0;
}


#include <iostream>
using namespace std;

#ifndef CONTADOR_H 
#define CONTADOR_H 

// Se define la clase y los atributos publicos y privados
class Contador {
	// Atributos privados 
	private:
	int frases = 0;

	// Atributos publicos con su respectivo constructor
	public:
	Contador();
	Contador(int frases);
	
        /* Se crean los métodos get and set al igual que las funciones que permiten el funcionamiento del programa*/
        int get_frases();
        void set_frases(int frases);
        void completar_arreglo();
        void contar_mayusculas();
        void contar_minusculas();
};
#endif
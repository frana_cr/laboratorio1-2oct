# LAB 1 - EJERCICIO 1 "SUMA DEL CUADRADO DE UN NUMERO"

## Comenzando
El proposito de este ejercicio es realizar un codigo en lenguaje C++, mediante la implementacion de clases crear un programa el cual permita llenar un areglo de caracter unidimensional con numeros enteros para luego ejecutar la accion de calcular la suma del cuadrado de los numeros, cabe destacar que la medida del arreglo de N elementos.

El programa se crea mediante el uso de clases, en este clase mediante la clase calculos se efectua en su mayoria la operatoria de la suma al cuadrado de los numero y tambien se procede a la construccion del arreglo, se declaran los get y set de todos los atributos que se declaran a la clase. Se crean dos funciones principales en las cuales se procede a llenar el arreglo mediante una estructura for, los numeros son ingresados de manera aleatoria por un rand esta funcion permite que se seleccione un rango aleatorio de numeros hasta el que se decida en este caso por eleccion propia se escogio hasta el numero 15. Luego se llevo a cabo la otra funcion en la cual se llevan a cabo los calculos en donde se recorre el arreglo y se van sumando los numeros de esta manera luego es posible calcular el cuadrado de dicha suma, multiplicando estos dos valores. Finalmente en el archivo Programa se le pregunta al usuario el tamaño para la construccion del arreglo y tambien se imprime cual es el valor final de las sumas se añade  "int tamano_arreglo = atoi(argv[1])" porque es el argumento en las lineas variables. 
A modo critico el archivo Calculos.h funciona de manera optima y compila, cumpliendo su objetivo de ser la base del archivo Calculos.cpp, este segundo archivo presenta ciertas fallas en las dos ultimas funciones descritas con anterioridad pero no en la declaracion del get y set se cree que el error puede estar en la funcion completar_arreglo() por lo cual impide que el programa funcione de manera optima y como se espera, el archivo programa permite que los datos sean impresos pero cuando se agrega esta ultima funcion produce que no compile y se presenten errores. 

## Prerequisitos
Sistema operativo Linux versión igual o superior a 18.04
Editor de texto (vim o geany)
### Instalacion
Para poder ejecutar el programa se debe conocer que versión de Ubuntu presenta la computadora. Ejecutar este comando:
lsb_release -a (versión de Ubuntu)
En donde:
Se puede corroborar qué versión se tiene instalada actualmente, debido a que se pueden presentar problemas si esta no es compatible con la aplicación que se está desarrollando.
Para descargar un editor de texto como vim se puede descargar de la siguiente manera:
sudo apt install vim
En donde: Por medio de este editor de texto se puede construir el codigo.
En el caso del desarrollo del trabajo se implemento el editor de texto Geany, descargado de Ubuntu Software.


### Ejecutando Pruebas
Cada archivo se puede ejecutar para ver si tiene errores por medio del comando en terminal g++ Nombre del archivo.cpp -o Nombre Archivo.
## Construido con
Ubuntu: Sistema operativo.
C++: Lenguaje de programación.
Geany: Editor de código.

## Versiones
Versiones de herramientas:
Ubuntu 20.04 LTS
Geany 1.36-1build1
Versiones del desarrollo del codigo: https://gitlab.com/frana_cr/laboratorio1-2oct/-/tree/master/Ejercicio%201T

## Autores
Francisca Castillo - Desarrollo del código y proyecto, narración README.

## Expresiones de gratitud
A los ejemplos en la plataforma de Educandus de Alejandro Valdes:  https://lms.educandus.cl/mod/lesson/view.php?id=730534 
Recursos web: http://aprende.olimpiada-informatica.org/cpp-int-operaciones 
https://www.programarya.com/Cursos/C++/Estructuras-de-Datos/Arreglos-o-Vectores

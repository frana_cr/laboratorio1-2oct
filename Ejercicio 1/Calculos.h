// Se agrega el include, la clase , gracias al using es mucho mas eficiente
#include <iostream>
using namespace std;

#ifndef CALCULOS_H
#define CALCULOS_H
// SE ANADE LA CLASE CALCULOS CON SUS ATRIBUTOS PUBLICOS Y PRIVADOS
class Calculos {
	// Atributos privados 
	private:
	int numeros = 0;
	int tamano_arreglo = 0;
	int suma = 0;
	// Atributos publicos con su respectivo constructor
	public:
	Calculos();
	Calculos(int numeros, int tamano_arreglo, int suma);
	
        /* Se crean los métodos get and set */
        int get_numeros();
        int get_tamano_arreglo();
        int get_suma();
        void set_numeros(int numeros);
        void set_tamano_arreglo(int tamano_arreglo);
        void set_suma(int suma);
        void completar_arreglo();
        void suma_cuadrados();
};
#endif
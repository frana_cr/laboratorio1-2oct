/*
 * g++ Ejercicio1.cpp -o Calculos
 * 
 * o ejecutar:
 * 
 * make
 */
 
// SE LLAMA AL ARCHIVO.H 
#include <iostream>
using namespace std;
#include "Calculos.h"

// SE LLAMA A LA CLASE Y LOS ATRIBUTOS DE ELLOS
Calculos::Calculos(int numeros, int tamano_arreglo, int suma) {
	this-> numeros = numeros;
	this-> tamano_arreglo = tamano_arreglo;
	this -> suma = suma;
}

// SE GENERAN LOS GET Y SET DE LAS VARIABLES

// se genera el get de los numeros
int Calculos::get_numeros() {
    return this-> numeros;
}
// se genera el get de tamaño del arreglo
int Calculos::get_tamano_arreglo() {
    return this-> tamano_arreglo;
}
// se genera el get de la suma
int Calculos:: get_suma(){
	return this-> suma;
}
// se genera el set de los numeros
void Calculos::set_numeros(int numeros) {
    this-> numeros = numeros;
}
// se genera el set de tamano arreglo
void Calculos::set_tamano_arreglo(int tamano_arreglo) {
    this-> tamano_arreglo = tamano_arreglo;
}
// se crea el set de la suma 
void Calculos::set_suma(int suma){
	this-> suma = suma;
}

// Funcion que completa el arreglo
void completar_arreglo(){
	for (i = 1; i < tamano_arreglo ; i++) {
		// SE GENERA UN RANDOM HASTA EL NUMERO 15
		numeros[i] = numerosrand()%16;
	}
}
// COMPLETAR FUNCION QUE HAGA LOS CALCULOS E IMPRIMA Y PASARLO AL MAIN 
void suma_cuadrado(){
	int suma_cuadrado = 0;
	// PRIMERO SE RECORRE
	for (i = 1; i < tamano_arreglo ; i++) {
		// SE CALCULA LA SUMA
		suma += numeros[i];
	}
	//FINALMENTE EL RESULTADO ES LA SUMA AL CUADRADO
	suma_cuadrado = suma * suma;
	return suma_cuadrado;
}
/*
 * g++ Clientes.cpp -o Clientes
 * 
 * o ejecutar:
 * 
 * make
 */
 // Se llaman a los archivos .h a implementar
#include <iostream>
#include <stdbool.h>
using namespace std;
#include "Clientes.h"

// Se define la clase y sus atributos 
Clientes::Clientes (string nombre, string telefono, int saldo, bool morosidad) {
    this-> nombre = nombre;
    this-> telefono = telefono;
    this-> saldo = saldo;
    this-> morosidad = morosidad;
}

// Se definen los metodos get y set de cada atributo 
// Se define el get de nombre
string Clientes::get_nombre() {
    return this-> nombre;
}
// Se define el get del telefono
string Clientes::get_telefono() {
    return this-> telefono;
}

// se define el get de saldo
int Clientes::get_saldo() {
    return this-> saldo;
}
// se define el get de morosidad
bool Clientes::get_morosidad() {
    return this-> morosidad;
}
// se crea el set del nombre
void Clientes::set_nombre(string nombre) {
    this->nombre = nombre;
}
// se crea el set del telefono 
void Clientes::set_telefono(string telefono) {
    this->telefono = telefono;
}
// se crea el set de saldo
void Clientes::set_saldo(int saldo) {
    this->saldo = saldo;
}
// se crea el set de morosidad
void Clientes::set_morosidad(bool morosidad ) {
    this-> morosidad = morosidad;
}
// Se le solicitan los datos a los usuarios
void solicitar_datos(){
	// SE SOLICITA EL NOMBRE Y SE INGRESA
	cout << "Nombre: ";
    getline(cin, nombre);
    // SE SOLICITA EL TELEFONO Y SE INGRESA
    cout << "Telefono: ";
    getline(cin, telefono);
    // SE SOLICITA AL CLIENTE QUE INGRESE EL SALDO 
    cout << "Saldo: ";
    getline(stoi(cin, saldo));

}
// Por medio de esta funcion se puede ver si el cliente es moroso o no 
void estado_cliente(){
	// SE LE SOLICITA AL USUARIO SI ES MOROSO POR UN TRUE O FALSE
	cout << "Estado morosidad: ";
    getline(cin, saldo);
    // SI EL USUARIO INGRESA SI 
    if(morosidad == True)
    {
        // se imprime que es un cliente con deuda y se añade al atributo
		cout << "Cliente con deuda";
		morosidad;
	}
    // De lo contrario es un cliente sin deuda
	else
	{
		cout << "Cliente sin deuda";
	}
}

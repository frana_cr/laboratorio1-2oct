#include <iostream>
using namespace std;

#ifndef CLIENTES_H 
#define CLIENTES_H 
// Se define la clase cliente con sus atributos publicos y privados
class Clientes {
	// Atributos privados
    private:
        string nombre;
        string telefono;
        int saldo;
        bool morosidad;
	// Atributos publicos
    public:
        /* constructores */
        Clientes ();
        Clientes(string nombre, string telefono, int saldo, bool morosidad);
        
        /* métodos get and set */
        string get_nombre();
        string get_telefono();
        int get_saldo();
        bool get_morosidad;
        void set_nombre(string nombre);
        void set_telefono(string telefono);
        void set_saldo(int saldo);
        void set_morosidad(bool morosidad);
        // Se definen las funciones principales del programa
        void solicitar_datos();
		void estado_cliente();
};
#endif